package reflection;
import java.lang.reflect.*;
import java.lang.ref.*;
public class inter {
	
	 public static void main(String arg[]) 
	 {
		 Class clase;
		 Field campo, campos[];
		 Method metodo, metodos[];
		 try {
		 
		 clase = Class.forName("reflection.Ejemplo");
		 
		 System.out.println("Lista de campos:\n");
		 campos = clase.getFields();
		 for (int i=0; i < campos.length; i++) {
		 campo = campos[i];
		 System.out.println("\t" + campo.getName());
		 }
		 
		 System.out.println("\nLista de metodos:\n");
		 metodos = clase.getMethods();
		 for (int i=0; i < metodos.length; i++) {
		 metodo = metodos[i];
		 System.out.println("\t" + metodo.getName());
		 }
		 
		 } catch (ClassNotFoundException e) {
			 System.out.println("No se ha encontrado la clase. " );
		 }
	}
		 
}
