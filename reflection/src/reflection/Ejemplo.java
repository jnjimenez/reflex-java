package reflection;

public class Ejemplo {
	public String nombre = "nicolas";
	 public String apellido = "jimenez";
	 private String direccion = "Leoncio Prado P- 1";
	 
	 private void setNombre(String s) 
	 {
		 nombre = s;
	 }
	 
	 private void setApellido(String a)
	 {
		 apellido = a;
	 }
	 
	 protected String getNombre()
	 {
		 return nombre;
	 }
	 
	 public String getApellido()
	 {
		 return apellido;
	 }
	 public void setDireccion(String d) {
		 direccion = d;
	 }
	 
	 public String getDireccion() {
	 return direccion;
	 }
}
